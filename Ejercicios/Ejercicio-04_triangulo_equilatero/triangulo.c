#include<stdio.h>
int main(){
int lado, resultado;

printf("\t\t-----Triangulo equilatero-----\n\t\t(Todos los lados son iguales)\n");
printf("Ingresa la longitud del lado: ");
scanf("%i", &lado);

resultado = lado * 3;

printf("El perimetro del triangulo es: %i", resultado);
return 0;
}