numero = int(input("Ingresa un numero entre el 1 y 50: "))
if numero >= 1 and numero <= 50:
    suma = 0
    for i in range(1,numero+1):
        suma=suma+i
    print("La suma de los numeros consecutivos hasta: ", numero, "es: ", suma)
else:
    print('numero invalido')
